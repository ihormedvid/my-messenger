package com.example.mymessage

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_receive_message.*

class ReceiveMessageActivity : AppCompatActivity() {


    // Java

    // public static final String EXTRA_MESSAGE = "message";

    companion object {
        val EXTRA_MESSAGE = "message" // -> Імя додаткового значення передаваємого в інтент
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_receive_message)

        val intent = getIntent() // -> повертає інтент,запустившої активності
        var messageText: String = intent.getStringExtra(EXTRA_MESSAGE) // -> Получити Інтент та вилкчити з нього повідомлення getStringExtra()
        var messageView = message
        messageView.setText(messageText) // -> Добавити текст в надпись с індетифікаторо message

    }
}

