package com.example.mymessage

import android.content.Intent // -> Необхідно імпортувати клас інтента в android.content.Intent, так як він використовується в onSendMessage()
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_create_message.*

class CreateMessageActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState) // Метод onCreate() -> Викликається при створенні Активності
        setContentView(R.layout.activity_create_message)
    }

    // Викликається onSendMessage() при виклику на кнопці
    fun onSendMessage(v: View) {

        var messageView = message // -> Присвоїти EditText з id message

        var messengeText: String = messageView.text.toString() // -> Получити текст із текствого поля з id message

        // 1-й параметр -> Вказує з від якого обєкта поступив інтент(для позначення текущої активності використовуєть слово this)
        // 2-й параметр -> Передається імя активності яка має получити intent
//        val intent = Intent(this, ReceiveMessageActivity::class.java)

        // putExtra -> Передає додаткову інфу в відправленому повідомленні
        // 1-й парметр -> Імя ресурса для передачі інформації
        // 2-й парметр -> саме значення
//        intent.putExtra(ReceiveMessageActivity.EXTRA_MESSAGE, messengeText) // -> Cтворення інтетнту,пілся добавлення в нього тексту.
        // -> В якоісті іменні додат інфи,використовується константа - в цьому випадку можна бути впевними,що CreateMessageActivity і ReceiveMessageActivity
        // -> Використовують одну і туже строку
        startActivity(intent) // -> Запускає Активність вказану в інтенті


        // Параметр -> тип дії виконання активності (ACTION_SEND) -> Для відправки повідомлень
        var intent = Intent(Intent.ACTION_SEND)

        intent.setType("text/pain") // -> Виклик каже Android що активність має вміти обробляти данні з типом MIME text/plain

        // 2-й параметр(messangeText) -> Відправлений текст
        intent.putExtra(Intent.EXTRA_TEXT, messengeText)
        var choserTitle: String = getString(R.string.chooser) // -> Получити текст заголовка
        var choseIntent = Intent.createChooser(intent, choserTitle) // -> Вивести діалогове вікно вибору
//        startActivity(intent)
        startActivity(choseIntent) // -> Запусити діалогове вікно вибору


    }
}
